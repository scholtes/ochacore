package me.scholtes.ochacore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;
import me.scholtes.ochacore.Utils;

public class ToggleSpeed implements CommandExecutor {

	OchaCore plugin;
	DataHandler dataHandler;
	
	
	public ToggleSpeed(OchaCore plugin, DataHandler dataHandler) {
		this.dataHandler = dataHandler;
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage(Utils.color("&cYou must be a player to do this!"));
		}
		Player p = (Player) sender;
		
		if (plugin.speedOff == null || plugin.speedOff.isEmpty() || !plugin.speedOff.contains(p.getUniqueId())) {
			sender.sendMessage(Utils.color(plugin.getConfig().getString("toggle_speed_off")));
			plugin.speedOff.add(p.getUniqueId());
			p.setWalkSpeed(0.2f);
			return true;
		}
		
		sender.sendMessage(Utils.color(plugin.getConfig().getString("toggle_speed_on")));
		plugin.speedOff.remove(p.getUniqueId());
		double dexteritySpeed = dataHandler.getDexterity(p) * (plugin.getConfig().getDouble("general_stats.dexterity.percent_for_extra_speed") / 100);
		if (dexteritySpeed > 0.8) dexteritySpeed = 0;
		p.setWalkSpeed(0.2f + (float) dexteritySpeed);
		return true;
	}

}
