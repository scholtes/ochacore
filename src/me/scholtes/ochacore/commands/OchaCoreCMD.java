package me.scholtes.ochacore.commands;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;
import me.scholtes.ochacore.Utils;

public class OchaCoreCMD implements CommandExecutor {
	
	OchaCore plugin;
	DataHandler dataHandler;
	
	public OchaCoreCMD(OchaCore plugin, DataHandler dataHandler) {
		this.plugin = plugin;
		this.dataHandler = dataHandler;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission("ochacore.admin")) {
			sender.sendMessage(Utils.color("&cYou can't do this!"));
			return true;
		}
		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reloadConfig();
				sender.sendMessage(Utils.color("&aConfig values reloaded!"));
				for (Player p : Bukkit.getOnlinePlayers()) {
					double percentHealth = p.getHealth() / p.getMaxHealth();
					double constitutionHealth = dataHandler.getConstitution(p) * (plugin.getConfig().getDouble("general_stats.constitution.percent_for_extra_health") / 100);
					int baseHealth = plugin.getConfig().getInt("base_health");
					p.setMaxHealth(baseHealth + (baseHealth * constitutionHealth));
					p.setHealth(p.getMaxHealth() * percentHealth);
					p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(4 + (4 * (plugin.getConfig().getDouble("general_stats.strength.percent_for_less_atkspeed") / 100 * dataHandler.getStrength(p))));
					double dexteritySpeed = dataHandler.getDexterity(p) * (plugin.getConfig().getDouble("general_stats.dexterity.percent_for_extra_speed") / 100);
					if (dexteritySpeed > 0.8) dexteritySpeed = 0;
					p.setWalkSpeed(0.2f + (float) dexteritySpeed);
				}
				return true;
			}
		}
		sender.sendMessage(Utils.color("&8----------------------"));
		sender.sendMessage(Utils.color("&6&lAvailable commands:"));
		sender.sendMessage(Utils.color("&e/ochacore reload"));
		sender.sendMessage(Utils.color("&8----------------------"));
		return true;
	}
	

}
