package me.scholtes.ochacore;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class DataHandler implements Listener {

	public final String INSERT = "INSERT INTO GeneralStats VALUES(?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE UUID=?";

	OchaCore plugin;

	public DataHandler(OchaCore plugin) {
		this.plugin = plugin;
	}

	private Map<UUID, Integer> constitution = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> strength = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> dexterity = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> intelligence = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> wisdom = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> charisma = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> maxHealth = new HashMap<UUID, Integer>();
	private Map<UUID, Integer> health = new HashMap<UUID, Integer>();

	// Health and Max Health

	public void cachePlayerHealth(Player p, int amount) {
		health.put(p.getUniqueId(), amount);
	}

	public void uncacheHealth(Player p) {
		health.remove(p.getUniqueId());
	}
	
	public int getHealth(Player p) {
		return health.get(p.getUniqueId());
	}

	public void cachePlayerMaxHealth(Player p, int amount) {
		maxHealth.put(p.getUniqueId(), amount);
	}
	
	public void uncacheMaxHealth(Player p) {
		maxHealth.remove(p.getUniqueId());
	}
	
	public int getMaxHealth(Player p) {
		return maxHealth.get(p.getUniqueId());
	}
	
	// Constitution

	public void cachePlayerConstitution(Player p, int amount) {
		constitution.put(p.getUniqueId(), amount);
	}

	public void uncacheConstitution(Player p) {
		constitution.remove(p.getUniqueId());
	}

	public void modifyConstitution(Player p, int amount) {
		constitution.put(p.getUniqueId(), constitution.get(p.getUniqueId()) + amount);
	}

	public int getConstitution(Player p) {
		return constitution.get(p.getUniqueId());
	}

	// Strength

	public void cachePlayerStrength(Player p, int amount) {
		strength.put(p.getUniqueId(), amount);
	}

	public void uncacheStrength(Player p) {
		strength.remove(p.getUniqueId());
	}

	public void modifyStrength(Player p, int amount) {
		strength.put(p.getUniqueId(), strength.get(p.getUniqueId()) + amount);
	}

	public int getStrength(Player p) {
		return strength.get(p.getUniqueId());
	}

	// Dexterity

	public void cachePlayerDexterity(Player p, int amount) {
		dexterity.put(p.getUniqueId(), amount);
	}

	public void uncacheDexterity(Player p) {
		dexterity.remove(p.getUniqueId());
	}

	public void modifyDexterity(Player p, int amount) {
		dexterity.put(p.getUniqueId(), dexterity.get(p.getUniqueId()) + amount);
	}

	public int getDexterity(Player p) {
		return dexterity.get(p.getUniqueId());
	}

	// Intelligence

	public void cachePlayerIntelligence(Player p, int amount) {
		intelligence.put(p.getUniqueId(), amount);
	}

	public void uncacheIntelligence(Player p) {
		intelligence.remove(p.getUniqueId());
	}

	public void modifyIntelligence(Player p, int amount) {
		intelligence.put(p.getUniqueId(), intelligence.get(p.getUniqueId()) + amount);
	}

	public int getIntelligence(Player p) {
		return intelligence.get(p.getUniqueId());
	}

	// Wisdom

	public void cachePlayerWisdom(Player p, int amount) {
		wisdom.put(p.getUniqueId(), amount);
	}

	public void uncacheWisdom(Player p) {
		wisdom.remove(p.getUniqueId());
	}

	public void modifyWisdom(Player p, int amount) {
		wisdom.put(p.getUniqueId(), wisdom.get(p.getUniqueId()) + amount);
	}

	public int getWisdom(Player p) {
		return wisdom.get(p.getUniqueId());
	}

	// Wisdom

	public void cachePlayerCharisma(Player p, int amount) {
		charisma.put(p.getUniqueId(), amount);
	}

	public void uncacheCharisma(Player p) {
		charisma.remove(p.getUniqueId());
	}

	public void modifyCharisma(Player p, int amount) {
		charisma.put(p.getUniqueId(), charisma.get(p.getUniqueId()) + amount);
	}

	public int getCharisma(Player p) {
		return charisma.get(p.getUniqueId());
	}

	public void loadStats(Player p) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {

				try {
					PreparedStatement insert = MySQL.getConnection().prepareStatement(INSERT);
					PreparedStatement selectConstitution = MySQL.getConnection().prepareStatement("SELECT CONSTITUTION FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectStrength = MySQL.getConnection().prepareStatement("SELECT STRENGTH FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectDexterity = MySQL.getConnection().prepareStatement("SELECT DEXTERITY FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectIntelligence = MySQL.getConnection().prepareStatement("SELECT INTELLIGENCE FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectWisdom = MySQL.getConnection().prepareStatement("SELECT WISDOM FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectCharisma = MySQL.getConnection().prepareStatement("SELECT CHARISMA FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectMaxHealth = MySQL.getConnection().prepareStatement("SELECT MAXHEALTH FROM GeneralStats WHERE UUID=?");
					PreparedStatement selectHealth = MySQL.getConnection().prepareStatement("SELECT HEALTH FROM GeneralStats WHERE UUID=?");
					insert.setString(1, p.getUniqueId().toString());
					insert.setInt(2, 0);
					insert.setInt(3, 0);
					insert.setInt(4, 0);
					insert.setInt(5, 0);
					insert.setInt(6, 0);
					insert.setInt(7, 0);
					insert.setInt(8, 100);
					insert.setInt(9, 100);
					insert.setString(10, p.getUniqueId().toString());
					insert.execute();

					selectConstitution.setString(1, p.getUniqueId().toString());
					selectStrength.setString(1, p.getUniqueId().toString());
					selectDexterity.setString(1, p.getUniqueId().toString());
					selectIntelligence.setString(1, p.getUniqueId().toString());
					selectWisdom.setString(1, p.getUniqueId().toString());
					selectCharisma.setString(1, p.getUniqueId().toString());
					selectMaxHealth.setString(1, p.getUniqueId().toString());
					selectHealth.setString(1, p.getUniqueId().toString());

					ResultSet resultConstitution = selectConstitution.executeQuery();
					if (resultConstitution.next()) {
						cachePlayerConstitution(p, resultConstitution.getInt("CONSTITUTION"));
					}
					resultConstitution.close();

					ResultSet resultStrength = selectStrength.executeQuery();
					if (resultStrength.next()) {
						cachePlayerStrength(p, resultStrength.getInt("STRENGTH"));
					}
					resultStrength.close();

					ResultSet resultDexterity = selectDexterity.executeQuery();
					if (resultDexterity.next()) {
						cachePlayerDexterity(p, resultDexterity.getInt("DEXTERITY"));
					}
					resultDexterity.close();

					ResultSet resultIntelligence = selectIntelligence.executeQuery();
					if (resultIntelligence.next()) {
						cachePlayerIntelligence(p, resultIntelligence.getInt("INTELLIGENCE"));
					}
					resultIntelligence.close();

					ResultSet resultWisdom = selectWisdom.executeQuery();
					if (resultWisdom.next()) {
						cachePlayerWisdom(p, resultWisdom.getInt("WISDOM"));
					}
					resultWisdom.close();

					ResultSet resultCharisma = selectCharisma.executeQuery();
					if (resultCharisma.next()) {
						cachePlayerCharisma(p, resultCharisma.getInt("CHARISMA"));
					}
					resultCharisma.close();
					
					ResultSet resultMaxHealth = selectMaxHealth.executeQuery();
					if (resultMaxHealth.next()) {
						cachePlayerMaxHealth(p, resultMaxHealth.getInt("MAXHEALTH"));
					}
					resultMaxHealth.close();
					
					ResultSet resultHealth = selectHealth.executeQuery();
					if (resultHealth.next()) {
						cachePlayerHealth(p, resultHealth.getInt("HEALTH"));
					}
					resultHealth.close();
					
					if (plugin.speedOff == null || !plugin.speedOff.contains(p.getUniqueId())) {
						double dexteritySpeed = getDexterity(p) * (plugin.getConfig().getDouble("general_stats.dexterity.percent_for_extra_speed") / 100);
						if (dexteritySpeed > 0.8) dexteritySpeed = 0;
						p.setWalkSpeed(0.2f + (float) (dexteritySpeed));
					}
					
					double constitutionHealth = getConstitution(p) * (plugin.getConfig().getDouble("general_stats.constitution.percent_for_extra_health") / 100);
					int baseHealth = plugin.getConfig().getInt("base_health");
					p.setMaxHealth(baseHealth + (baseHealth * constitutionHealth));
					p.setHealth(getHealth(p));
					
					p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(4 + (4 * (plugin.getConfig().getDouble("general_stats.strength.percent_for_less_atkspeed") / 100 * getStrength(p))));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}

	public void saveStats(Player p, boolean async) {
		if (async) {
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					saveStatements(p);
				}
			});
		} else
			saveStatements(p);
	}

	public void saveStatements(Player p) {
		executeGeneralStatementSave(p, "CHARISMA");
		executeGeneralStatementSave(p, "CONSTITUTION");
		executeGeneralStatementSave(p, "DEXTERITY");
		executeGeneralStatementSave(p, "INTELLIGENCE");
		executeGeneralStatementSave(p, "STRENGTH");
		executeGeneralStatementSave(p, "WISDOM");
		executeGeneralStatementSave(p, "HEALTH");
		executeGeneralStatementSave(p, "MAXHEALTH");
			
		uncacheCharisma(p);
		uncacheConstitution(p);
		uncacheDexterity(p);
		uncacheIntelligence(p);
		uncacheStrength(p);
		uncacheWisdom(p);
		uncacheHealth(p);
		uncacheMaxHealth(p);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		p.setHealthScale(20);
		cachePlayerCharisma(p, 0);
		cachePlayerConstitution(p, 0);
		cachePlayerDexterity(p, 0);
		cachePlayerIntelligence(p, 0);
		cachePlayerStrength(p, 0);
		cachePlayerWisdom(p, 0);
		cachePlayerHealth(p, 100);
		cachePlayerMaxHealth(p, 100);
		loadStats(p);
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		saveStats(e.getPlayer(), true);
	}

	@SuppressWarnings("deprecation")
	public void executeGeneralStatementSave(Player p, String column) {

		try {
			PreparedStatement statement = MySQL.getConnection().prepareStatement("UPDATE GeneralStats SET " + column + "=? WHERE UUID=?");
			if (column.equalsIgnoreCase("CHARISMA")) statement.setInt(1, getCharisma(p));
			if (column.equalsIgnoreCase("CONSTITUTION")) statement.setInt(1, getConstitution(p));
			if (column.equalsIgnoreCase("DEXTERITY")) statement.setInt(1, getDexterity(p));
			if (column.equalsIgnoreCase("INTELLIGENCE")) statement.setInt(1, getIntelligence(p));
			if (column.equalsIgnoreCase("STRENGTH")) statement.setInt(1, getStrength(p));
			if (column.equalsIgnoreCase("WISDOM")) statement.setInt(1, getWisdom(p));
			if (column.equalsIgnoreCase("HEALTH")) statement.setInt(1, (int) p.getHealth());
			if (column.equalsIgnoreCase("MAXHEALTH")) statement.setInt(1, (int) p.getMaxHealth());
			statement.setString(2, p.getUniqueId().toString());
			statement.execute();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
