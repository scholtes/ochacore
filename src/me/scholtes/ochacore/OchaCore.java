package me.scholtes.ochacore;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import me.scholtes.ochacore.commands.OchaCoreCMD;
import me.scholtes.ochacore.commands.ToggleSpeed;
import me.scholtes.ochacore.events.DamageEvent;
import me.scholtes.ochacore.generalstats.GeneralStatsGUI;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class OchaCore extends JavaPlugin {

	DataHandler dataHandler = new DataHandler(this);

	public List<UUID> speedOff = new ArrayList<UUID>();
	
	public void onEnable() {

		getConfig().options().copyDefaults(true);
		saveConfig();
		
		getServer().getPluginManager().registerEvents(new DamageEvent(this, dataHandler), this);
		getServer().getPluginManager().registerEvents(dataHandler, this);
		getServer().getPluginManager().registerEvents(new GeneralStatsGUI(dataHandler, this), this);

		getCommand("generalstats").setExecutor(new GeneralStatsGUI(dataHandler, this));
		getCommand("ochacore").setExecutor(new OchaCoreCMD(this, dataHandler));
		getCommand("togglespeed").setExecutor(new ToggleSpeed(this, dataHandler));
		
		MySQL.connect();
		
		new BukkitRunnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					String actionBar = getConfig().getString("action_bar");
					p.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder(Utils.color(actionBar.replaceAll("%health%", String.valueOf((int) p.getHealth())).replaceAll("%maxhealth%", String.valueOf((int) p.getMaxHealth())))).create());
				}
			}
		}.runTaskTimer(this, 0L, 40L);
		
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					if (MySQL.getConnection() != null && !MySQL.getConnection().isClosed()) {
						MySQL.getConnection().createStatement().execute("SELECT 1");
					}
				} catch (SQLException e) {
					MySQL.connect();
				}
			}
		}.runTaskTimerAsynchronously(this, 60 * 20, 60 * 20);
		PreparedStatement ps;
		try {
			ps = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS GeneralStats (UUID VARCHAR(36), CONSTITUTION int, STRENGTH int, DEXTERITY int, INTELLIGENCE int, WISDOM int, CHARISMA int, MAXHEALTH int, HEALTH int, PRIMARY KEY (UUID))");
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void onDisable() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			dataHandler.saveStats(p, false);
		}
		MySQL.disconnect();
	}

}
