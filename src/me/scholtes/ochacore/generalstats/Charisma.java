package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public class Charisma extends Ability {
	
	public Charisma(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		super(name, player, plugin, dataHandler);
	}

	@Override
	public List<String> getEffects() {
		List<String> effects = new ArrayList<String>();
		if (getLevel() == 0) {
			effects.add("Reach level 1 to reveal");
			effects.add("Reach level 1 to reveal");
		}
		else {
			double rounded = Math.round(plugin.getConfig().getDouble("general_stats.charisma.percent_for_extra_dodge") * getLevel() * 100.0) / 100.0;
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.charisma.percent_for_extra_damage") * getLevel()) + "% damage");
			effects.add("+" + String.valueOf(rounded) + "% dodge");
		}
		return effects;
	}

	@Override
	public int getLevel() {
		return getDataHandler().getCharisma(getPlayer());
	}

	@Override
	public void addLevel(int level) {
		getDataHandler().modifyCharisma(getPlayer(), level);
		
	}

}
