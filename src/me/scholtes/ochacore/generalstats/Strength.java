package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public class Strength extends Ability {
	
	public Strength(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		super(name, player, plugin, dataHandler);
	}

	@Override
	public List<String> getEffects() {
		List<String> effects = new ArrayList<String>();
		if (getLevel() == 0) {
			effects.add("Reach level 1 to reveal");
			effects.add("Reach level 1 to reveal");
		}
		else {
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.strength.percent_for_extra_damage") * getLevel()) + "% damage");
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.strength.percent_for_less_atkspeed") * getLevel()) + "% attack speed");
		}
		return effects;
	}

	@Override
	public int getLevel() {
		return getDataHandler().getStrength(getPlayer());
	}

	@Override
	public void addLevel(int level) {
		getDataHandler().modifyStrength(getPlayer(), level);
		getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(4 + (4 * (plugin.getConfig().getDouble("general_stats.strength.percent_for_less_atkspeed") / 100 * dataHandler.getStrength(getPlayer()))));
	}

}
