package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public class Intelligence extends Ability {
	
	public Intelligence(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		super(name, player, plugin, dataHandler);
	}

	@Override
	public List<String> getEffects() {
		List<String> effects = new ArrayList<String>();
		if (getLevel() == 0) {
			effects.add("Reach level 1 to reveal");
			effects.add("Reach level 7 to reveal");
		}
		else if (getLevel() < 7) {
			effects.add("-" + String.valueOf(plugin.getConfig().getDouble("general_stats.intelligence.percent_for_less_spelltime") * getLevel()) + "% spell casting time");
			effects.add("Reach level 7 to reveal");
		}
		else {
			effects.add("-" + String.valueOf(plugin.getConfig().getDouble("general_stats.intelligence.percent_for_less_spelltime") * getLevel()) + "% spell casting time");
			effects.add("+" + String.valueOf(plugin.getConfig().getInt("general_stats.intelligence.spell_slot.amount") * (int) (getLevel() / plugin.getConfig().getInt("general_stats.intelligence.spell_slot.every_level"))) + " spell slots");
		}
		return effects;
	}

	@Override
	public int getLevel() {
		return getDataHandler().getIntelligence(getPlayer());
	}

	@Override
	public void addLevel(int level) {
		getDataHandler().modifyIntelligence(getPlayer(), level);
		
	}

}
