package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public class Constitution extends Ability {
	
	public Constitution(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		super(name, player, plugin, dataHandler);
	}

	@Override
	public List<String> getEffects() {
		List<String> effects = new ArrayList<String>();
		if (getLevel() == 0) {
			effects.add("Reach level 1 to reveal");
			effects.add("Reach level 1 to reveal");
		}
		else {
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.constitution.percent_for_extra_health") * getLevel()) + "% health");
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.constitution.percent_for_extra_damage") * getLevel()) + "% damage");
		}
		return effects;
	}

	@Override
	public int getLevel() {
		return getDataHandler().getConstitution(getPlayer());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void addLevel(int level) {
		getDataHandler().modifyConstitution(getPlayer(), level);
		double percentHealth = getPlayer().getHealth() / getPlayer().getMaxHealth();
		double constitutionHealth = dataHandler.getConstitution(getPlayer()) * (plugin.getConfig().getDouble("general_stats.constitution.percent_for_extra_health") / 100);
		int baseHealth = plugin.getConfig().getInt("base_health");
		getPlayer().setMaxHealth(baseHealth + (baseHealth * constitutionHealth));
		getPlayer().setHealth(getPlayer().getMaxHealth() * percentHealth);
	}

}
