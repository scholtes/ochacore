package me.scholtes.ochacore.generalstats;

import java.util.List;

import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public abstract class Ability {
	
	protected String name;
	protected Player player;
	protected OchaCore plugin;
	protected DataHandler dataHandler;
	
	public Ability(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		this.name = name;
		this.player = player;
		this.dataHandler = dataHandler;
		this.plugin = plugin;
	}
	
	public abstract List<String> getEffects();
	public abstract int getLevel();
	public abstract void addLevel(int level);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public OchaCore getPlugin() {
		return plugin;
	}

	public void setPlugin(OchaCore plugin) {
		this.plugin = plugin;
	}

	public DataHandler getDataHandler() {
		return dataHandler;
	}

	public void setDataHandler(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}
	
	
}
