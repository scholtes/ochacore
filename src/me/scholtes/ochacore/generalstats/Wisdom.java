package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public class Wisdom extends Ability {
	
	public Wisdom(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		super(name, player, plugin, dataHandler);
	}

	@Override
	public List<String> getEffects() {
		List<String> effects = new ArrayList<String>();
		if (getLevel() == 0) {
			effects.add("Reach level 1 to reveal");
			effects.add("Reach level 1 to reveal");
		}
		else {
			effects.add("-" + String.valueOf(plugin.getConfig().getDouble("general_stats.wisdom.percent_for_less_spelltime") * getLevel()) + "% spell casting time");
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.wisdom.percent_for_extra_dodge") * getLevel()) + "% dodge");
		}
		return effects;
	}

	@Override
	public int getLevel() {
		return getDataHandler().getWisdom(getPlayer());
	}

	@Override
	public void addLevel(int level) {
		getDataHandler().modifyWisdom(getPlayer(), level);
		
	}

}
