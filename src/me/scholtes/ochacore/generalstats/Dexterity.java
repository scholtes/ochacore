package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;

public class Dexterity extends Ability {
	
	public Dexterity(String name, Player player, OchaCore plugin, DataHandler dataHandler) {
		super(name, player, plugin, dataHandler);
	}

	@Override
	public List<String> getEffects() {
		List<String> effects = new ArrayList<String>();
		if (getLevel() == 0) {
			effects.add("Reach level 1 to reveal");
			effects.add("Reach level 1 to reveal");
		}
		else {
			double rounded = Math.round(plugin.getConfig().getDouble("general_stats.charisma.percent_for_extra_dodge") * getLevel() * 10.0) / 10.0;
			effects.add("+" + String.valueOf(rounded) + "% speed");
			effects.add("+" + String.valueOf(plugin.getConfig().getDouble("general_stats.dexterity.percent_for_extra_dodge") * getLevel()) + "% dodge");
		}
		return effects;
	}

	@Override
	public int getLevel() {
		return getDataHandler().getDexterity(getPlayer());
	}

	@Override
	public void addLevel(int level) {
		getDataHandler().modifyDexterity(getPlayer(), level);
		if (plugin.speedOff == null || !plugin.speedOff.contains(getPlayer().getUniqueId())) {
			double dexteritySpeed = dataHandler.getDexterity(getPlayer()) * (plugin.getConfig().getDouble("general_stats.dexterity.percent_for_extra_speed") / 100);
			if (dexteritySpeed > 0.8) dexteritySpeed = 0;
			getPlayer().setWalkSpeed(0.2f + (float) dexteritySpeed);
		}
	}

}
