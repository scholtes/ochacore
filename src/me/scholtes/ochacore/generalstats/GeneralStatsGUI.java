package me.scholtes.ochacore.generalstats;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;
import me.scholtes.ochacore.Utils;

public class GeneralStatsGUI implements CommandExecutor, Listener {

	DataHandler dataHandler;
	OchaCore plugin;
	
	public GeneralStatsGUI(DataHandler dataHandler, OchaCore plugin) {
		this.dataHandler = dataHandler;
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(Utils.color("&cOnly a player can run this command!"));
			return true;
		}
		
		Player p = (Player) sender;
		Inventory inv = Bukkit.createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', "&4&lGeneral Stats"));
		List<String> lore = new ArrayList<String>();
		for (int i = 0; i < 45; i++) {
			inv.setItem(i, Utils.guiItem("", lore, Material.BLACK_STAINED_GLASS_PANE));
		}

		Strength strength = new Strength("Strength", p, plugin, dataHandler);
		Constitution constitution = new Constitution("Constitution", p, plugin, dataHandler);
		Charisma charisma = new Charisma("Charisma", p, plugin, dataHandler);
		Intelligence intelligence = new Intelligence("Intelligence", p, plugin, dataHandler);
		Wisdom wisdom = new Wisdom("Wisdom", p, plugin, dataHandler);
		Dexterity dexterity = new Dexterity("Dexterity", p, plugin, dataHandler);
		
		inv.setItem(11, statsItem(constitution, Material.REDSTONE, false));
		inv.setItem(13, statsItem(strength, Material.DIAMOND_SWORD, false));
		inv.setItem(15, statsItem(dexterity, Material.FEATHER, false));
		inv.setItem(29, statsItem(intelligence, Material.GOLD_NUGGET, false));
		inv.setItem(31, statsItem(wisdom, Material.KNOWLEDGE_BOOK, false));
		inv.setItem(33, statsItem(charisma, Material.POPPY, false));
		lore.add("");
		lore.add(Utils.color("&7Click to close the inventory!"));
		inv.setItem(44, Utils.guiItem(Utils.color("&c&nClose"), lore, Material.BARRIER));
		p.openInventory(inv);
		p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
		
		return true;
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		
		if (!e.getView().getTitle().equals(Utils.color("&4&lGeneral Stats"))) {
			return;
		}
		e.setCancelled(true);
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) return;
		
		String itemName = e.getCurrentItem().getItemMeta().getDisplayName();
		Player p = (Player) e.getWhoClicked();
		
		if (itemName == null) return;
		
		Strength strength = new Strength("Strength", p, plugin, dataHandler);
		Constitution constitution = new Constitution("Constitution", p, plugin, dataHandler);
		Charisma charisma = new Charisma("Charisma", p, plugin, dataHandler);
		Intelligence intelligence = new Intelligence("Intelligence", p, plugin, dataHandler);
		Wisdom wisdom = new Wisdom("Wisdom", p, plugin, dataHandler);
		Dexterity dexterity = new Dexterity("Dexterity", p, plugin, dataHandler);
		
		ItemStack item = new ItemStack(Material.DIRT);
		boolean levelup = false;
		if (itemName.equals(Utils.color("&6&nConstitution"))) {
			levelup = true;
			item = statsItem(constitution, Material.REDSTONE, true);
		}
		if (itemName.equals(Utils.color("&6&nStrength"))) {
			levelup = true;
			item = statsItem(strength, Material.DIAMOND_SWORD, true);
		}
		if (itemName.equals(Utils.color("&6&nCharisma"))) {
			levelup = true;
			item = statsItem(charisma, Material.POPPY, true);
		}
		if (itemName.equals(Utils.color("&6&nWisdom"))) {
			levelup = true;
			item = statsItem(wisdom, Material.KNOWLEDGE_BOOK, true);
		}
		if (itemName.equals(Utils.color("&6&nIntelligence"))) {
			levelup = true;
			item = statsItem(intelligence, Material.GOLD_NUGGET, true);
		}
		if (itemName.equals(Utils.color("&6&nDexterity"))) {
			levelup = true;
			item = statsItem(dexterity, Material.FEATHER, true);
		}
		if (levelup) {
			p.getOpenInventory().getTopInventory().setItem(e.getSlot(), item);
			p.updateInventory();
		}
		
	}
	
	private ItemStack statsItem(Ability ability, Material mat, boolean levelup) {
		List<String> lore = new ArrayList<String>();
		int level = ability.getLevel();
		if (levelup && !(level >= 50) && (ability.getPlayer().getLevel() >= level + 1)) {
			level += 1;
			ability.addLevel(1);
			
			int xpToRemove = Utils.getTotalExperience(ability.getPlayer()) - Utils.getEXPRequired(level);
			Utils.setTotalExperience(ability.getPlayer(), xpToRemove);
			ability.getPlayer().playSound(ability.getPlayer().getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
		}
		
		lore.add(Utils.color(""));
		lore.add(Utils.color("&6&l>&e&l> &fLevel"));
		lore.add(Utils.color("&c* &7&o" + level));
		lore.add(Utils.color(""));
		lore.add(Utils.color("&6&l>&e&l> &fAbility"));
		for (String s : ability.getEffects()) {
			lore.add(Utils.color("&c* &7&o" + s));
		}
		lore.add(Utils.color(""));
		lore.add(Utils.color("&6&l>&e&l> &fLevel-Up Cost"));
		if (level >= 50) {
			lore.add(Utils.color("&c&nMax Level!"));
		}
		else {
			lore.add(Utils.color("&c* &7&o" + (level + 1) + " levels"));
			if (ability.getPlayer().getLevel() >= level + 1) lore.add(Utils.color("&a&nClick to level up!"));
			else lore.add(Utils.color("&c&nNot enough levels!"));
		}
		ItemStack item = Utils.guiItem(Utils.color("&6&n" + ability.getName()), lore, mat);
		if (level != 0) {
			item.setAmount(level);
			ItemMeta meta = item.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 1, false);
	        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
	        item.setItemMeta(meta);
		}
		return item;
	}
	
	
}
