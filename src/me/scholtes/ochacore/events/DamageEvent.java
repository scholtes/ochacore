package me.scholtes.ochacore.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.scholtes.ochacore.DataHandler;
import me.scholtes.ochacore.OchaCore;
import me.scholtes.ochacore.Utils;

public class DamageEvent implements Listener {
	
	OchaCore plugin;
	DataHandler dataHandler;
	
	public DamageEvent(OchaCore plugin, DataHandler dataHandler) {
		this.plugin = plugin;
		this.dataHandler = dataHandler;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamageStrength(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player victim = (Player) e.getEntity();
			double charismaDodge = (plugin.getConfig().getDouble("general_stats.charisma.percent_for_extra_dodge") / 100) * dataHandler.getCharisma(victim);
			double dexterityDodge = (plugin.getConfig().getDouble("general_stats.dexterity.percent_for_extra_dodge") / 100) * dataHandler.getDexterity(victim);
			if (Math.random() < (dexterityDodge + charismaDodge)) {
				if (e.getEntity() instanceof Player) {
					e.setCancelled(true);
					victim.sendMessage(Utils.color(plugin.getConfig().getString("dodge_message")));
				}
			}
		}
		if (e.getDamager() instanceof Player) {
			Player damager = (Player) e.getDamager();
			double strengthDamage = (plugin.getConfig().getDouble("general_stats.strength.percent_for_extra_damage") / 100) * dataHandler.getStrength(damager);
			double constitutionDamage = (plugin.getConfig().getDouble("general_stats.constitution.percent_for_extra_damage") / 100) * dataHandler.getConstitution(damager);
			double charismaDamage = (plugin.getConfig().getDouble("general_stats.charisma.percent_for_extra_damage") / 100) * dataHandler.getCharisma(damager);
			e.setDamage(e.getDamage() + (e.getDamage() * (strengthDamage + constitutionDamage + charismaDamage)));
			
		}
		
	}

}
